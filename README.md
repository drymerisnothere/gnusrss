<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgheadline22">1. gnusrss</a>
<ul>
<li><a href="#orgheadline10">1.1. English</a>
<ul>
<li><a href="#orgheadline1">1.1.1. About</a></li>
<li><a href="#orgheadline2">1.1.2. Features</a></li>
<li><a href="#orgheadline3">1.1.3. Requirements</a></li>
<li><a href="#orgheadline4">1.1.4. Git repository</a></li>
<li><a href="#orgheadline5">1.1.5. Install</a></li>
<li><a href="#orgheadline6">1.1.6. Configuration</a></li>
<li><a href="#orgheadline7">1.1.7. Crontab</a></li>
<li><a href="#orgheadline8">1.1.8. Use with twitter2rss and/or GNU Social</a></li>
<li><a href="#orgheadline9">1.1.9. License</a></li>
</ul>
</li>
<li><a href="#orgheadline21">1.2. Castellano</a>
<ul>
<li><a href="#orgheadline11">1.2.1. Acerca de</a></li>
<li><a href="#orgheadline12">1.2.2. Features</a></li>
<li><a href="#orgheadline13">1.2.3. Requisitos</a></li>
<li><a href="#orgheadline14">1.2.4. Repositorio git</a></li>
<li><a href="#orgheadline15">1.2.5. Instalación</a></li>
<li><a href="#orgheadline16">1.2.6. Configuración</a></li>
<li><a href="#orgheadline17">1.2.7. Crontab</a></li>
<li><a href="#orgheadline18">1.2.8. Uso con twitter2rss y/o GNU Social</a></li>
<li><a href="#orgheadline19">1.2.9. Bugs</a></li>
<li><a href="#orgheadline20">1.2.10. Licencia</a></li>
</ul>
</li>
</ul>
</li>
</ul>
</div>
</div>

# gnusrss<a id="orgheadline22"></a>

## English<a id="orgheadline10"></a>

### About<a id="orgheadline1"></a>

gnusrss parse feeds and post them to GNU Social. The idea of ​​this program came from [spigot](https://github.com/nathans/spigot), a program that posts feeds to the social network [pump.io](https://pump.io) as does gnusrss but better, because it controls the possible flood. gnusrss does not have this option and it will be managed with the crontab (for now).

### Features<a id="orgheadline2"></a>

-   Multiple feed and GNU Social accounts support
-   sqlite3 is used to store the feeds
-   Can fetch RSS files or url indistinctly
-   Twitter image upload support when used with [twitter2rss](http://daemons.cf/cgit/twitter2rss)

### Requirements<a id="orgheadline3"></a>

Need a version equal to or greater than python 3 and some libraries:

-   [feedparser](//pypi.python.org/pypi/feedparser) >= 5.0
-   [requests](https://pypi.python.org/pypi/requests/2.11.1) >= 2.11.1

### Git repository<a id="orgheadline4"></a>

It's in two places:

-   <http://daemons.cf/cgit/gnusrss>: the original repository
-   <https://notabug.org/drymer/gnusrss/>: A mirror in which it can be put issues and feature requests

### Install<a id="orgheadline5"></a>

As with any program that uses python, it should be used a virtual environment (virtualenv), but that is user selectable. It's possible to use one of the next installation methods:

Install via pip:

    $ su -c "pip3 install gnusrss"

Clone the repository:

    $ git clone git://daemons.cf/gnusrss
    # OR ...
    $ git clone https://notabug.org/drymer/gnusrss/
    $ cd gnusrss
    $ su -c "pip3 install -r requirements.txt"
    $ su -c "python3 setup.py install"

If on parabola:

    $ su -c "pacman -S gnusrss"

### Configuration<a id="orgheadline6"></a>

The program is (or should be) quite intuitive. Running the following, should show the basics:

    $ gnusrss.py
    usage: gnusrss [-h] [-c file_name] [-C] [-p config_file] [-P] [-k file_name]

    Post feeds to GNU Social

    optional arguments:
        -h, --help            show this help message and exit
        -c file_name, --create-config file_name
                        creates a config file
        -C, --create-db       creates the database
        -p config_file, --post config_file
                        posts feeds
        -P, --post-all        posts all feeds
        -k file_name, --populate-database file_name
                        fetch the RSS and save it in the database

In any case, if not clear, read the following.

For the first use, it must be created the database and the first configuration file. This can done using the same command, like this:

    $ gnusrss.py --create-db --create-config daemons

Then it will ask several questions to create the first configuration file. It should look like this:

    Database created!
    Hi! Now we'll create config file!
    Please enter the feed's URL: https://daemons.cf/rss.xml
    Please enter your username (user@server.com): drymer@quitter.se
    Please enter your password: falsePassword
    Do you need to shorten the URLs that you 'post? Please take in account
    That You should only use it if your node only have 140 characters.
    Answer with "yes" or just press enter if you do not want to use it:
    Please enter your feed's fallbackurl. If you do not want or have one,
    just press enter:
    Now we're gona fetch the feed. Please wait ...
    Done! The tags are:
       tags
       title_detail
       link
       authors
       links
       author_detail
       published_parsed
       title
       summary
       id
       author
       published
       guidislink
       summary_detail
    The XML has-been parsed. Choose wich format you want:
    Please put the tags inside the square brackets
    Ex: {title} - {link} by @{author}: {title} - {link} by @{author}
    Do you want to allow insecure connection to your GNU social server?
    Answer with "yes" or just press enter if you don't want to use it:
    Do you want to populate the database? (y) Or you prefer to post old items? (n)

The file is saved under the name 'daemons.ini'. It should look like this:

    [Feeds]
    feed = https://daemons.cf/rss.xml
    user = drymer@quitter.se
    password = falsePassword
    shorten =
    fallback_feed =
    format = {title} - {link} by @ {author}

It can create all the configuration files you want.
When creating the above file, it put into the database all the feeds that had so far. Thus, when running **gnusrss** for the first time, it will not post nothing to GNU Social until the feed has new information.
To post feeds from a concrete config file or all execute, respectively, the following:

    $ gnusrss.py -p daemons.ini
    $ gnusrss.py -P

If the config file is created manually and the user don't want to post all the feed's content, just use the &#x2013;populate-database option to save them to the database:

    $ gnusrss.py -k otherFile.ini

### Crontab<a id="orgheadline7"></a>

The recommended way to execute **gnurss** is using the crontab. Each time it's run, a single item of the feed will be posted to prevent flood. Depending on the number of feeds that are published, you should choose a different runtime. For a blog that publishs once a day, it could be used the following:

    $ crontab -e
    # A file is open and gets the following
    * 12 * * * cd $rutaDEgnusrss && gnusrss.py -p daemons.ini

So it runs once, every day at midday. If, however, it's used with [twitter2rss](http://daemons.cf/cgit/twitter2rss/), it could be recommended putting it to run every five minutes. It has to be remembered that is important to run in the directory where the database was created, because is where it will search it..

### Use with twitter2rss and/or GNU Social<a id="orgheadline8"></a>

It works like any feed, except for the field that is published. In both you have to choose `{summary}`. An example configuration file is as follows:

    [feeds]
    feed = https://quitter.se/api/statuses/user_timeline/127168.atom
    user = drymer@quitter.se
    password = falsePassword
    shorten =
    fallback_feed =
    format = {summary}

The feed can be achieved by looking at the source code of the page of the account you want. For [twitter2rss](http://daemons.cf/cgit/twitter2rss), you can host it or can use this [web](http://daemons.cf/twitter2rss).

### License<a id="orgheadline9"></a>

    This program is free software: you can redistribute it and / or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, Either version 3 of the License, or
    (At your option) any later version.

    This program is distributed in the hope That it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    Along With This Program. If not, see <http://www.gnu.org/licenses/>.

## Castellano<a id="orgheadline21"></a>

### Acerca de<a id="orgheadline11"></a>

gnusrss parsea feeds y los postea en GNU Social. La idea de hacer este programa surgió de [spigot](https://github.com/nathans/spigot), un programa que postea feeds en la red social [pump.io](https://pump.io) igual que hace gnusrss pero mejor, ya que controla el posible flood. gnusrss no tiene esta opción y se controlará con el propio crontab (de momento).

### Features<a id="orgheadline12"></a>

-   Soporta múltiples feeds y cuentas de GNU Social
-   sqlite3 es usado para guardar los feeds
-   Se puede usar tanto archivos RSS cómo url indistintamente
-   Soporta la súbida de imágenes de Twitter cuando es usado en conjunto con [twitter2rss](http://daemons.cf/cgit/twitter2rss)

### Requisitos<a id="orgheadline13"></a>

Necesita una versión de python igual o superior a la 3 y algunas librerias:

-   [feedparser](https://pypi.python.org/pypi/feedparser) >= 5.0
-   [requests](https://pypi.python.org/pypi/requests/2.11.1) >= 2.11.1

### Repositorio git<a id="orgheadline14"></a>

Está en dos sitios:

-   <http://daemons.cf/cgit/gnusrss>: el repositorio original
-   <https://notabug.org/drymer/gnusrss/>: un mirror, en el que se pueden poner los problemas y sugerencias de mejoras

### Instalación<a id="orgheadline15"></a>

Cómo con cualquier programa con python, es recomendable usar un entorno virtual (virtualenv), pero eso queda a elección del usuario. Se puede escoger entre los siguientes metodos:

Instalar usando pip:

    $ su -c "pip3 install gnusrss"

Clonar el repositorio:

    $ git clone git://daemons.cf/gnusrss
    # O ...
    $ git clone https://notabug.org/drymer/gnusrss/
    $ cd gnusrss
    $ su -c "pip3 install -r requirements.txt"
    $ su -c "python3 setup.py install"

Si se usa parabola:

    $ su -c "pacman -S gnusrss"

### Configuración<a id="orgheadline16"></a>

El programa es (o debería ser) bastante intuitivo. Ejecutando lo siguiente, deberia verse lo básico:

    $ gnusrss.py
    usage: gnusrss [-h] [-c file_name] [-C] [-p config_file] [-P] [-k file_name]

    Post feeds to GNU Social

    optional arguments:
        -h, --help            show this help message and exit
        -c file_name, --create-config file_name
                        creates a config file
        -C, --create-db       creates the database
        -p config_file, --post config_file
                        posts feeds
        -P, --post-all        posts all feeds
        -k file_name, --populate-database file_name
                        fetch the RSS and save it in the database

En cualquier caso, si no queda claro, leer lo siguiente.

Para el primer uso, la base de datos y el primer archivo de configuración deben ser creados. Podemos hacerlo usando la misma orden, tal que así:

    $ gnusrss.py --create-db --create-config daemons

A continuación hará varias preguntas para configurar el primer archivo de configuración. Debería verse así:

    Database created!
    Hi! Now we'll create de config file!
    Please introduce the feed's url: https://daemons.cf/rss.xml
    Please introduce your username (user@server.com): drymer@quitter.se
    Please introduce your password: contraseñaFalsa
    {1}Do you need to shorten the urls that you post? Please take in account
    that you should only use it if your node only has 140 characters.
    Answer with "yes" or just press enter if you don't want to use it:
    {2}Please introduce your feed's fallbackurl. If you don't want or have one,
    just press enter:
    Now we're gona fetch the feed. Please wait...
    Done! The tags are:
       tags
       title_detail
       link
       authors
       links
       author_detail
       published_parsed
       title
       summary
       id
       author
       published
       guidislink
       summary_detail
    The XML has been parsed. Choose wich format you want:
    {3}Please put the tags inside the square brackets
    Ex: {title} - {link} by @{author}: {title} - {link} by @{author}
    {4}Do you want to allow insecure connection to your GNU social server?
    Answer with "yes" or just press enter if you don't want to use it:
    {5}Do you want to populate the database? (y) Or you prefer to post old items? (n)

A continuación traduciré las lineas con los números entre corchetes.
{1} Necesitas acortar las url que quieres postear? Por favor ten en cuenta que sólo deberia usarse si el servidor sólo tiene 140 carácteres.
{2} Por favor introduce tu feed de emergencia. Si no tienes uno, solamente aprieta enter.
{3} Por favor pon las etiquetas dentro de los corchetes.
{4} Quieres permitir conexiones inseguras a tu servidor GNU social? Responde con "si" o simplemente apreta enter si no necesitas usarlo.
{5} Quieres llenar la base de datos? (y) O prefieres publicar los artículos antiguos? (n)

Respecto al 3, hay que examinar el código fuente del RSS para saber cuales interesan. En general, el que hay de ejemplo será lo que se busque. En el caso 4, sólo es útil si el servidor usa un certificado auto-firmado.

El archivo se guardará con el nombre 'daemons.ini'. Después de todas estas preguntas, debería verse similar a esto:

    [feeds]
    feed = https://daemons.cf/rss.xml
    user = drymer@quitter.se
    password = contraseñaFalsa
    shorten =
    fallback_feed =
    format = {title} - {link} by @{author}
    insecure =

Se pueden crear todos los archivos de configuración que se quieran.
Al haber creado el archivo anterior, se han metido en la base de datos todos los feeds que habian hasta el momento. Por lo tanto, cuando se ejecuta **gnusrss** por primera vez, no posteará nada en GNU Social, a menos que el feed tenga nueva información.
Para postear los feeds de un archivo o todos, ejecutar, respectivamente, lo siguiente:

    $ gnusrss.py -p daemons.ini
    $ gnusrss.py -P

Si el archivo de configuración ha sido creado manualmente y no se quiere postear el contenido del feed, sólo hay que ejecutar la opción &#x2013;populate-database para guardar estos en la base de datos:

    $ gnusrss.py -k otherFile.ini

### Crontab<a id="orgheadline17"></a>

El modo recomendado de ejecución de gnusrss es usando el crontab. Cada vez que se ejecute posteará un sólo elemento del feed para evitar el flood. Según la cantidad de feeds que se publiquen, se deberia escoger un tiempo de ejecución distinto. Para un blog que publique una vez al día, con poner lo siguiente, deberia valer:

    $ crontab -e
    # Se abrirá un archivo y se mete lo siguiente
    * 12 * * * cd $rutaDEgnusrss && gnusrss.py -p daemons.cf

Así se ejecuta una vez al día, a las doce de la mañana. Si, en cambio, lo usasemos con [twitter2rss](http://daemons.cf/cgit/twitter2rss/), se recomienda poner que se ejecute cada cinco minutos. Hay que recordar que es importante que se ejecute en el directorio en el que se ha creado la base de datos, ya que es ahí dónde la buscará.

### Uso con twitter2rss y/o GNU Social<a id="orgheadline18"></a>

Funciona igual que con cualquier feed, exceptuando el campo que se publica. En ambos hay que escoger `{summary}`. Un ejemplo de archivo de configuración sería el siguiente:

    [feeds]
    feed = https://quitter.se/api/statuses/user_timeline/127168.atom
    user = drymer@quitter.se
    password = contraseñaFalsa
    shorten =
    fallback_feed =
    format = {summary}

El feed se puede conseguir mirando el código fuente de la página de la cuenta que se quiere. En el caso de [twitter2rss](http://daemons.cf/cgit/twitter2rss), se puede hostear o se puede usar esta [web](http://daemons.cf/twitter2rss). No es recomendable usarlo con ningún nodo que use 140 carácteres, ya que en los retweets se añade un símbolo, "♻", lo cual hará que un tweet de 140 carácteres no sea posteado.

### Bugs<a id="orgheadline19"></a>

### Licencia<a id="orgheadline20"></a>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
